const captionList = [
  {
    caption: 'No meal without a deal🔥🔥, only on EazyDiner. ',
  },
  {
    caption: 'Get best deals only on @eazydiner 🔥. ',
  },
  {
    caption: 'Eazydiner is here to save your wallet. 🤩🤩 ',
  },
  {
    caption: 'Go Prime, avail super exciting prime deals. ',
  },
  {
    caption: 'Get your food delivered at your place 😋, book via @eazydiner. ',
  },
  {
    caption: 'Loved the deals 🤩🤩 by @eazydiner. ',
  },
  {
    caption: 'Enjoy your best dining experience 😋😋 ever with @eazydiner. ',
  },
  {
    caption: "A foodie's party never stops🤩🤩, enjoy‎ now via @eazydiner. ",
  },
  {
    caption: 'The only app that keeps the party on. 🔥 ',
  },
  {
    caption: 'The only app that will never let you down @eazydiner. ',
  },
  {
    caption: 'Avail the super exciting discounts through PayEazy. ',
  },
  {
    caption: 'Loved the app and deals🤩🤩😋 by @eazydiner. ',
  },
  {
    caption: 'Use @eazydiner for the best dining experience. ',
  },
  {
    caption: '@eazydiner is there to help you enjoy your food cravings. ',
  },
  {
    caption: 'Attention foodies!😋 enjoy heavy discounts only on @eazydiner. ',
  },
  {
    caption: 'Want to have a smooth dining experience? enjoy on @eazydiner. ',
  },
  {
    caption: 'Not only on festivals, enjoy exciting offers everyday. ',
  },
  {
    caption: 'Aisi deals kahin aur nahi milegi. 🤩🤩 ',
  },
  {
    caption: 'Go prime, you will never regret. 🔥🔥 ',
  },
  {
    caption: 'Get amazing deals, bank offers, coupons... only on @eazydiner. ',
  },
];

const emojiUnicodesList = [
  '0x1F60D',
  '0x1F929',
  '0x1F60B',
  '0x1F64F',
  '0x1F91F',
  '0x1F4A5',
  '0x1F9E1',
];

const createSocialSharePostLinks = () => {
  // taking captions randomly from the list
  let CaptionString = '';
  const captionIndex = Math.floor(Math.random() * captionList.length);
  const currentCaption = captionList[captionIndex];
  CaptionString += currentCaption.caption;

  // taking emojis randomly from the list
  const emojiIndex = Math.floor(Math.random() * emojiUnicodesList.length);
  const currentEmoji = emojiUnicodesList[emojiIndex];
  const emoji = String.fromCodePoint(currentEmoji);
  const emojiString = ' ' + emoji + ' ' + emoji;

  // div for social share buttons
  const socialShareButtonsDiv = document.getElementById(
    'eazydiner-food-trends-social-media-share-random-generation',
  );
  socialShareButtonsDiv.setAttribute('style', `margin-top:10px;`);

  // accessing data attributes for post url and title
  const postUrl = socialShareButtonsDiv.getAttribute('data-post-url');
  const postTitle = socialShareButtonsDiv.getAttribute('data-post-title');

  // creating link for twitter and onclick event handler function
  const twitterAnchor = document.createElement('a');

  // css styles for twitter anchor
  twitterAnchor.setAttribute(
    'style',
    `padding: 8px 10px;
    -o-transition: .5s;
    -ms-transition: .5s;
    -moz-transition: .5s;
    -webkit-transition: .5s;
    transition: .5s;
    background-color: #00aced;
    border-radius: 50%;
    cursor: pointer;`,
  );

  // style changes for twitter anchor mouse events: onmouseover and onmouseout
  twitterAnchor.onmouseover = () => {
    twitterAnchor.style.boxShadow = `
      0 3px 1px -2px rgba(0, 0, 0, 0.2),
      0 2px 2px 0 rgba(0, 0, 0, 0.20),
      0 1px 5px 0 rgba(0, 0, 0, 0.12)`;
  };

  twitterAnchor.onmouseout = () => {
    twitterAnchor.style.boxShadow = `
      0 3px 1px -2px rgba(0, 0, 0, 0.0),
      0 2px 2px 0 rgba(0, 0, 0, 0.0),
      0 1px 5px 0 rgba(0, 0, 0, 0.0)`;
  };

  // creating icon element for twitter
  const twitterIcon = document.createElement('i');

  // font awesome icon class for twitter
  twitterIcon.classList.add('fa');
  twitterIcon.classList.add('fa-twitter');

  // onclick event handler function for twitter
  twitterAnchor.onclick = () => {
    window.open(
      'https://twitter.com/intent/tweet?text=' +
        postTitle +
        '%0a@eazydiner' +
        emojiString +
        '%0a' +
        CaptionString +
        '%0a%0a' +
        postUrl,
      'Twitter',
      'width=1000,height=700',
    );
  };

  twitterAnchor.appendChild(twitterIcon);
  socialShareButtonsDiv.appendChild(twitterAnchor);

  // creating link for facebook and onclick event handler function
  const facebookAnchor = document.createElement('a');

  // css styles for facebook anchor
  facebookAnchor.setAttribute(
    'style',
    `margin-left: 5px;
    padding: 8px 13px;
    -o-transition:.5s;
    -ms-transition:.5s;
    -moz-transition:.5s;
    -webkit-transition:.5s;
    transition: .5s;
    background-color: #3B5998;
    border-radius: 50%;
    cursor: pointer;
    color: white;`,
  );

  // style changes for facebook anchor mouse events: onmouseover and onmouseout
  facebookAnchor.onmouseover = () => {
    facebookAnchor.style.boxShadow = `
      0 3px 1px -2px rgba(0, 0, 0, 0.2),
      0 2px 2px 0 rgba(0, 0, 0, 0.20),
      0 1px 5px 0 rgba(0, 0, 0, 0.12)`;
  };

  facebookAnchor.onmouseout = () => {
    facebookAnchor.style.boxShadow = `
    0 3px 1px -2px rgba(0, 0, 0, 0.0),
    0 2px 2px 0 rgba(0, 0, 0, 0.0),
    0 1px 5px 0 rgba(0, 0, 0, 0.0)`;
  };

  // creating icon element for facebook
  const facebookIcon = document.createElement('i');

  // font awesome icon class for facebook
  facebookIcon.classList.add('fa');
  facebookIcon.classList.add('fa-facebook');

  // onclick event handler function for facebook
  facebookAnchor.onclick = () => {
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=' +
        postUrl +
        '&quote=' +
        postTitle +
        '&hashtag=%23eazydiner',
      'facebook-share-dialog',
      'width=1000,height=800',
    );
  };

  facebookAnchor.appendChild(facebookIcon);
  socialShareButtonsDiv.appendChild(facebookAnchor);
};
